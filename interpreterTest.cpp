#include "interpreterTest.h"

class TestInterface : public CpuInterface {
public:
    bool isValidCodePointer(CodeSource codeSource, uint64_t addr, CpuState* state) override {
        return true;
    }

    // Called when a svc occurs. Returning false will halt emulation, true will continue
    bool Svc(uint32_t svc, CpuState* state) override {
        return false;
    }

    // Called to read or write system registers, respectively
    virtual uint64_t SR(uint32_t op0, uint32_t op1, uint32_t crn, uint32_t crm, uint32_t op2) override {
        return 0;
    }
    virtual void SR(uint32_t op0, uint32_t op1, uint32_t crn, uint32_t crm, uint32_t op2, uint64_t value) override { }

    virtual void Log(const std::string& message) override { }
    virtual void Error(const std::string& message) override { throw std::runtime_error(message); }
};

TestInterface testInterface;

extern "C" {

bool runSingleTest(uint64_t pc, uint32_t inst, CpuState* state, int pageCount, SinglePageMap *pages) {
    try {
        /*std::cout << "START" << std::endl;
        std::cout << "-----" << std::endl;
        for(auto i = 0; i < 32; ++i) {
            std::cout << "X" << std::dec << i << "=" << std::hex << state->X[i] << "  ";
            if((i & 1) == 1) std::cout << std::endl;
        }
        std::cout << "PC=" << std::hex << state->PC << "  " << "SP=" << state->SP << std::endl;
        std::cout << "-----" << std::dec << std::endl;*/
        Interpreter interpreter{&testInterface, state};
        for(auto i = 0; i < pageCount; ++i)
            interpreter.pageMap[pages[i].page] = pages[i].base;
        state->BranchTo = (ulong) -3L;
        /*std::cout << "-----" << std::endl;
        for(auto i = 0; i < 32; ++i) {
            std::cout << "X" << std::dec << i << "=" << std::hex << state->X[i] << "  ";
            if((i & 1) == 1) std::cout << std::endl;
        }
        std::cout << "PC=" << std::hex << state->PC << "  " << "SP=" << state->SP << std::endl;
        std::cout << "-----" << std::dec << std::endl;*/
        if(!interpreter.interpret(inst, state->PC = pc))
            return false;
        /*std::cout << "-----" << std::endl;
        for(auto i = 0; i < 32; ++i) {
            std::cout << "X" << std::dec << i << "=" << std::hex << state->X[i] << "  ";
            if((i & 1) == 1) std::cout << std::endl;
        }
        std::cout << "PC=" << std::hex << state->PC << "  " << "SP=" << state->SP << std::endl;
        std::cout << "-----" << std::dec << std::endl;*/
        if(state->BranchTo != (ulong) -3L) {
            state->PC = state->BranchTo;
            state->BranchTo = (ulong) -3L;
        }
        return true;
    } catch(...) {
        return false;
    }
}

}
