#pragma once

#include "interpreter.h"

struct SinglePageMap {
    uint64_t page;
    uint8_t* base;
};

extern "C" {
    EXPORTED bool runSingleTest(uint64_t pc, uint32_t inst, CpuState* state, int pageCount, SinglePageMap *pages);
}
